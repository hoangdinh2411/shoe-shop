## Build image
FROM maven:3.5.3-jdk-8-alpine as build

WORKDIR /app
COPY . . 
RUN mvn install -DskipTests=true

## Run stage

FROM openjdk:8-jre-alpine

COPY --from=build /app/target/shoe-ShoppingCart-0.0.1-SNAPSHOT.jar /run/shoe-ShoppingCart-0.0.1-SNAPSHOT.jar

EXPOSE 8080

ENTRYPOINT java -jar /run/shoe-ShoppingCart-0.0.1-SNAPSHOT.jar

